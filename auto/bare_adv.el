(TeX-add-style-hook
 "bare_adv"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("IEEEtran" "10pt" "journal" "compsoc")))
   (TeX-run-style-hooks
    "latex2e"
    "IEEEtran"
    "IEEEtran10"
    "graphicx"
    "color")
   (TeX-add-symbols
    "MYhyperrefoptions"))
 :latex)

